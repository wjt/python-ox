# vi:si:et:sw=4:sts=4:ts=4
# encoding: utf-8
__version__ = '1.0.0'

from . import imdb
from . import wikipedia
from . import google
from . import piratecinema
from . import oxdb
